package org.rhok.foodmovr.service;

import org.rhok.foodmovr.dao.UserDao;
import org.rhok.foodmovr.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * User: Maarten
 */
@org.springframework.stereotype.Service
@Transactional
public class UserService implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired
    private UserDao userDao;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent authenticationSuccessEvent) {
        String userName = ((UserDetails) authenticationSuccessEvent.getAuthentication().
                getPrincipal()).getUsername();
        User user = userDao.findByUsername(userName);
        user.setLastLogin(new Date());
    }
}
