package org.rhok.foodmovr.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.rhok.foodmovr.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * User: Maarten
 */
@Repository
public class UserDaoImpl extends AbstractDao<User> implements UserDao {
    /**
     * Instantiates a new dao with hibernate.
     */
    protected UserDaoImpl() {
        super(User.class);
    }

    @Override
    public User findByUsername(String username) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
        Root<User> user = criteriaQuery.from(User.class);
        criteriaQuery.select(user);
        criteriaQuery.where(builder.equal(user.get("username"), username));

        return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }
}
