package org.rhok.foodmovr.dao;

import java.util.Collection;

/**
 * The Interface Dao.
 * 
 * @param <T>
 *            the generic type
 */
public interface Dao<T> {

    /**
     * Create an entity and return it.
     * 
     * @param entity
     *            The entity that will be created.
     * @return The created object of type T
     */
    T create(T entity);

    /**
     * Gets an entity.
     * 
     * @param id
     *            The id of the entity that will be returned
     * @return The entity of type T.
     */
    T get(Long id);

    /**
     * Update an entity.
     * 
     * @param entity
     *            The entity that will be updated.
     */
    void update(T entity);

    /**
     * Delete an entity.
     * 
     * @param id
     *            The id of the entity that will be deleted.
     */
    void delete(Long id);

    /**
     * Get all entities.
     * 
     * @return Collection of all entities
     */
    Collection<T> getAll();
}
