package org.rhok.foodmovr.dao;

import org.rhok.foodmovr.model.User;

/**
 * User: Maarten
 */
public interface UserDao extends Dao<User> {

    User findByUsername(String username);
}
