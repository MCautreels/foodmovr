package org.rhok.foodmovr.web.service;

import org.rhok.foodmovr.model.exception.NotValidException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: Maarten
 * Date: 8/11/12
 * Time: 23:02
 * To change this template use File | Settings | File Templates.
 */
public interface WebService<T> {
    public T create(T entity) throws NotValidException;
    public T read(Long id);
    public void update(T entity) throws NotValidException;
    public void delete(Long id);
    public Collection<T> getAll();
}
