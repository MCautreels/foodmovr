package org.rhok.foodmovr.web.service;

import org.rhok.foodmovr.model.exception.NotValidException;
import org.rhok.foodmovr.service.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Maarten
 * Date: 8/11/12
 * Time: 23:04
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractWebService<T> implements WebService<T> {

    @Override
    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public @ResponseBody T create(@RequestBody T entity) throws NotValidException {
        return getService().create(entity);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody T read(@PathVariable("id") Long id) {
        return getService().get(id);
    }

    @Override
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public @ResponseBody void update(@RequestBody() T entity) throws NotValidException {
        getService().edit(entity);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody void delete(@PathVariable("id") Long id) {
        getService().delete(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody Collection<T> getAll() {
        return getService().getAll();
    }

    @ExceptionHandler(NotValidException.class)
    public Map<String, String> notValid(NotValidException exception){
        Map<String, String> map = new HashMap<String, String>();
        map.put("statusCode", "400");
        map.put("statusMessage", exception.getMessage());

        return map;
    }

    public abstract Service<T> getService();
}
