

var genericControllers = angular.module('generic.controllers', []);

genericControllers.controller('NavigationCtrl', [
    '$scope', '$location',
    function($scope, $location) {
        $scope.isActive = function(viewLocation) {
            return viewLocation === $location.path();
        }
    }
]);