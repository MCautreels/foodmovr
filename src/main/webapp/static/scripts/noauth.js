'use strict';

/*
 * Unauthorized Module
 */
var noAuth = angular.module('noAuth', [
    'noAuth.controllers',
    'generic.controllers',
    'ngRoute'
]);

noAuth.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/home', {
                templateUrl: baseUrl + 'static/views/noauth/home.html',
                controller: 'StaticContentCtrl'
            }).
            when('/about', {
                templateUrl: baseUrl + 'static/views/noauth/about.html',
                controller: 'StaticContentCtrl'
            }).
            when('/contact', {
                templateUrl: baseUrl + 'static/views/noauth/contact.html',
                controller: 'ContactCtrl'
            }).
            when('/comingup', {
                templateUrl: baseUrl + 'static/views/noauth/comingup.html',
                controller: 'StaticContentCtrl'
            }).
            when('/login', {
                templateUrl: baseUrl + 'static/views/noauth/login.html',
                controller: 'StaticContentCtrl'
            }).
            when('/register', {
                templateUrl: baseUrl + 'static/views/noauth/register.html',
                controller: 'StaticContentCtrl'
            }).
            otherwise({
                redirectTo: '/home'
            });
    }]);