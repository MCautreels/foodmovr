'use strict';

/*
 * Authorized Module
 */
var foodMovr = angular.module('foodmovr', [
        'foodmovr.controllers',
        'ngRoute'
    ]);

foodMovr.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/home', {
                templateUrl: '/static/views/home.html',
                controller: 'HomeCtrl'
            }).
            otherwise({
                redirectTo: '/home'
            });
    }]);